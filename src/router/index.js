import Vue from 'vue'
import Router from 'vue-router'
import TheHome from '../pages/HomePage.vue';
import helpPage from '../pages/helpPage';
import productsPage from '../pages/productsPage';
import NewsPage from '../pages/NewsPage.vue';
import FormPage from '../pages/FormPage.vue';

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', component: TheHome },
    { path: '/help', component: helpPage },
    {path: '/catalog', component: productsPage},
    { path: '/news', component: NewsPage },
    { path: '/auth', component: FormPage }
  ]
})
