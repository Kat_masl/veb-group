import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import VueRouter from 'vue-router'
import router from './router/index'

import './scss/main.scss';
import './scss/print.css';
import './scss_s/style.scss'

Vue.use(VueRouter)

Vue.config.productionTip = false

import axios from 'axios';
Vue.prototype.axios = axios

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')

